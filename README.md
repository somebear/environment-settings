# Settings

## Dot files

The `dotfiles` directory contains my settings files that provide the environment that I want. All 
of the files map 1-to-1 to dot files in the home directory, e.g. `profile` maps to `~/.profile`.

## Completions

This directory contains any completion files. The `profile` sets up automatic sourcing of files in 
the directory `~/.bash_completion.d` any completions files can just be dropped into that directory.

## Scripts

Any useful scripts I write or come across.

### Reviewboard

The reviewboard script provides a single interface to the rbt tool that is more useful to me (e.g.
it automatically adds the engineering group as a reviewer). Call `./reviewboard -h` for usage.
